package io.androidpro.architect

import java.util.Date

/**
 * Data ready-to-be-displayed
 *
 * What can be changed here?
 * OrderStatus can be separate displayable
 * java.util.Date could be changed to String / localized String,
 * but at the moment we don't display Date on screen
 */
data class OrderListItemDisplayable(
    val orderId: String,
    val customerAddress: String,
    val restaurantName: String,
    val restaurantAddress: String,
    val orderItems: String,
    var orderStatus: OrderStatus,
    val pickupDate: Date
)
