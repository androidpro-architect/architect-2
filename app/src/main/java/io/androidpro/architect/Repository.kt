package io.androidpro.architect

import kotlinx.coroutines.delay
import kotlin.random.Random

/**
 * Created by jaroslawmichalik on 02/10/2023
 */
/**
 * Fake implementation of repository.
 * Returns randomly generated orders and restaurants with built-in delay.
 */
object FakeRepository {

    val orders = List(99) { i ->
        val orderId = i.toString()
        val customerName = "Customer $i"
        val customerAddress = "Address $i"
        val customerContact = "Contact $i"
        val restaurantName = "Restaurant ${Random.nextInt(1, 5)}"
        val restaurantAddress = "Restaurant Address ${Random.nextInt(1, 5)}"
        val orderItems = generateFakeOrderItems(Random.nextInt(1, 5))
        val specialInstructions = if (Random.nextBoolean()) "Special Instructions $i" else null
        val orderStatus = OrderStatus.values()[Random.nextInt(OrderStatus.values().size)]
        val timestamp = System.currentTimeMillis()

        Order(
            orderId,
            customerName,
            customerAddress,
            customerContact,
            restaurantName,
            restaurantAddress,
            orderItems,
            specialInstructions,
            orderStatus,
            timestamp
        )
    }

    suspend fun getAll(): List<Order> {
        delay(700)
        return orders
    }

    suspend fun getById(id: String): Order {
        delay(300)
        return orders.first { it.orderId == id }
    }

    private fun generateFakeOrderItems(numberOfItems: Int): List<OrderItem> {
        val random = Random

        return (1..numberOfItems).map { i ->
            val itemName = "Item $i"
            val quantity = random.nextInt(1, 5)
            val price = random.nextDouble(5.0, 20.0)

            OrderItem(itemName, quantity, price)
        }
    }
}
