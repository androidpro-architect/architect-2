package io.androidpro.architect

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import io.androidpro.architect.OrderListState.Data
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import java.util.Date

class OrderListViewModel(
    private val repo: FakeRepository
) : ViewModel() {

    /**
     * We have to have some initial state
     */
    private val _state = MutableStateFlow<OrderListState>(OrderListState.Loading)

    val state : StateFlow<OrderListState>
        get() = _state.asStateFlow()

    init {
        loadData()
    }

    private fun loadData() {

        /**
         * You can add try-catch here or runCatching to handle errors
         */

        viewModelScope.launch {
            val items = repo.getAll().map {
                /**
                 * Mapping from Order to OrderDisplayable can be extracted to separate class
                 */
                OrderListItemDisplayable(
                    orderId = it.orderId,
                    customerAddress = it.customerAddress,
                    /**
                     * No need to put all order items
                     * in this class, since we're displaying
                     * this list as string
                     */
                    orderItems = it.orderItems.joinToString(separator = ", ") { it.itemName },
                    orderStatus = it.orderStatus,
                    pickupDate = Date(it.timestamp),
                    restaurantAddress = it.restaurantAddress,
                    restaurantName = it.restaurantName
                )
            }

            _state.value = items.let(::Data)
        }
    }

}

/**
 * Class representing states that OrderListState can have.
 * Q: How it could look like if we wanted
 * to display loading and data at the same time?
 */
sealed interface OrderListState {
    object Loading : OrderListState
    object Error : OrderListState
    data class Data(val orders: List<OrderListItemDisplayable>): OrderListState
}