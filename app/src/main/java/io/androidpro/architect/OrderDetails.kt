package io.androidpro.architect

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Card
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

@Composable
fun OrderDetails(orderId: String, padding: PaddingValues) {

    var order by remember { mutableStateOf<Order?>(null) }

    LaunchedEffect(Unit) {
        val orderById = withContext(Dispatchers.IO) {
            FakeRepository.getById(orderId)
        }
        order = orderById
    }

    if (order != null) {
        // Display the order details
        Card(
            modifier = Modifier
                .fillMaxWidth()
                .padding(8.dp)
                .padding(padding)
        ) {

            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(16.dp),
                verticalArrangement = Arrangement.spacedBy(8.dp)
            ) {
                Text(
                    text = "Order ID: ${order!!.orderId}",
                    style = MaterialTheme.typography.headlineSmall
                )
                Text(text = "Customer: ${order!!.customerName}")
                Text(text = "Customer Address: ${order!!.customerAddress}")
                Text(text = "Customer Contact: ${order!!.customerContact}")
                Text(text = "Restaurant: ${order!!.restaurantName}")
                Text(text = "Restaurant Address: ${order!!.restaurantAddress}")
                Text(text = "Special Instructions: ${order!!.specialInstructions ?: "None"}")
                Text(text = "Order Status: ${order!!.orderStatus}")

                // Displaying Order Items
                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 16.dp),
                    verticalArrangement = Arrangement.spacedBy(4.dp)
                ) {
                    Text(text = "Items:", style = MaterialTheme.typography.headlineSmall)
                    order!!.orderItems.forEach { item ->
                        Text(text = "${item.quantity}x ${item.itemName} - \$${item.price}")
                    }
                }
            }
        }
    }
}