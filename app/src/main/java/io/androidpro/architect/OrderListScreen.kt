package io.androidpro.architect


import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Card
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun OrderListScreen() {

    val viewModel = viewModel {
        OrderListViewModel(repo = FakeRepository)
    }


    Scaffold(
        topBar = {
            TopAppBar(title = { Text("Marketplace") })
        }
    ) { padding ->
        OrderList(viewModel, padding)
    }
}


/**
 * Option 1: Create viewModel inside this component
 * Option 2: Inject viewModel to this component and let the higher component create VM
 */
@Composable
fun OrderList(viewModel: OrderListViewModel, padding: PaddingValues) {

    val state = viewModel.state.collectAsState()

    when (val currentState = state.value) {
        is OrderListState.Data -> OrderListItems(padding = padding, data = currentState.orders)
        OrderListState.Error -> TODO()
        OrderListState.Loading -> CircularProgressIndicator()
    }
}


/**
 * Widget that has no context of ViewModel, StateFlow -> we're passing only data to display (+modifier config for compose)
 */
@Composable
fun OrderListItems(padding: PaddingValues, data: List<OrderListItemDisplayable>) {
    LazyColumn(
        modifier = Modifier
            .fillMaxSize()
            .padding(padding),
        verticalArrangement = Arrangement.spacedBy(16.dp)
    ) {
        items(data) { order ->
            OrderItemCard(order) {
                //OrderList: clicked $order
            }
        }
    }
}


/**
 * Widget that has only Displayable + click callback
 */
@Composable
fun OrderItemCard(order: OrderListItemDisplayable, clicked: (OrderListItemDisplayable) -> Unit) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp)
            .clickable { clicked(order) }
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp),
            verticalArrangement = Arrangement.spacedBy(8.dp)
        ) {
            Text(
                text = "Order nr: ${order.orderId}",
                fontWeight = FontWeight.Bold,
                fontSize = 18.sp
            )
            Text(
                text = order.orderItems,
                fontWeight = FontWeight.Light,
                fontSize = 14.sp
            )
            /**
             * Could be localized. stringResource.
             */
            Text(text = "Deliver to: ${order.customerAddress}")
            Text(text = "Pickup from: ${order.restaurantName}")
            Text(text = "Status: ${order.orderStatus}")
        }
    }
}