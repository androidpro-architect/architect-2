package io.androidpro.architect

/**
 * Created by jaroslawmichalik on 02/10/2023
 */
data class Order(
    val orderId: String, // Unique identifier for the order
    val customerName: String, // Name of the customer who placed the order
    val customerAddress: String, // Address where the order needs to be delivered
    val customerContact: String, // Contact details of the customer
    val restaurantName: String, // Name of the restaurant where the order is placed
    val restaurantAddress: String, // Address of the restaurant
    val orderItems: List<OrderItem>, // List of items in the order
    val specialInstructions: String?, // Any special instructions provided by the customer
    var orderStatus: OrderStatus, // Current status of the order
    val timestamp: Long // Time when the order was placed
)

data class OrderItem(
    val itemName: String, // Name of the item
    val quantity: Int, // Quantity of the item ordered
    val price: Double // Price of the item
)

enum class OrderStatus {
    PLACED,
    ACCEPTED,
    EN_ROUTE,
    DELIVERED,
    CANCELLED
}
